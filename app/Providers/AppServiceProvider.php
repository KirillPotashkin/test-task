<?php

namespace App\Providers;

use App\Services\Magazine\CacheMagazineQueries;
use App\Services\Magazine\Contract\MagazineQueries;
use App\Services\Magazine\EloquentMagazineQueries;
use App\Services\Publisher\CachePublisherQueries;
use App\Services\Publisher\Contract\PublisherQueries;
use App\Services\Publisher\EloquentPublisherQueries;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(MagazineQueries::class,
            CacheMagazineQueries::class);

        $this->app
            ->when(CacheMagazineQueries::class)
            ->needs(MagazineQueries::class)
            ->give(EloquentMagazineQueries::class);

        //PublisherQueries
        $this->app->bind(PublisherQueries::class,
            CachePublisherQueries::class);

        $this->app
            ->when(CachePublisherQueries::class)
            ->needs(PublisherQueries::class)
            ->give(EloquentPublisherQueries::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
