<?php

namespace App\Http\Requests\Magazine;

use App\Services\Magazine\MagazineSearchDto;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MagazineSearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'publisher_id' => ['nullable', 'integer', Rule::exists('publishers', 'id')],
            'name'         => ['nullable', 'string'],
        ];
    }

    public function getDto(): MagazineSearchDto
    {
        return new MagazineSearchDto(
            $this->get('name'),
            $this->get('publisher_id'),
            request('page', 1)
        );
    }
}
