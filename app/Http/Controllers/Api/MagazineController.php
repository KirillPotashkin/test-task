<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Magazine\MagazineSearchRequest;
use App\Services\Magazine\MagazineHandler;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * Class MagazineController
 * @package App\Http\Controllers\Api
 */
class MagazineController extends Controller
{

    /**
     * MagazineController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api', [
            'except' => [
            ],
        ]);
    }

    /**
     * @OA\Get(
     * path="/api/magazines/search",
     *   tags={"magazines"},
     *   summary="Magazines Search",
     *   operationId="magazines search",
     *   security={{ "apiAuth": {} }},
     *
     *   @OA\Parameter(
     *      name="name",
     *      in="query",
     *      required=false,
     *      @OA\Schema(
     *           type="string"
     *      )
     *   ),
     *   @OA\Parameter(
     *      name="publisher_id",
     *      in="query",
     *      required=false,
     *      @OA\Schema(
     *          type="int"
     *      )
     *   ),
     *   @OA\Parameter(
     *      name="page",
     *      in="query",
     *      required=false,
     *      @OA\Schema(
     *          type="int"
     *      )
     *   ),
     *
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param  MagazineSearchRequest  $request
     * @param  MagazineHandler  $handler
     *
     * @return LengthAwarePaginator
     */
    public function search(MagazineSearchRequest $request, MagazineHandler $handler): LengthAwarePaginator
    {
        return $handler->searchExecute($request->getDto());
    }


    /**
     * @OA\Get(
     * path="/api/magazines/{id}",
     *   tags={"magazines"},
     *   summary="Magazines by ID",
     *   operationId="Magazines by ID",
     *   security={{ "apiAuth": {} }},
     *
     *   @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     *   ),
     *
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param  MagazineHandler  $handler
     * @param  string  $id
     *
     * @return Builder|Builder[]|Collection|Model|null
     */
    public function show(MagazineHandler $handler, string $id)
    {
        return $handler->getByIdExecute((int) $id);
    }
}
