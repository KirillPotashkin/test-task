<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Publisher;
use App\Services\Publisher\EloquentPublisherQueries;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class PublisherController
 * @package App\Http\Controllers\Api
 */
class PublisherController extends Controller
{

    /**
     * PublisherController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api', [
            'except' => [
            ],
        ]);
    }


    /**
     * @OA\Get(
     * path="/api/publishers/list",
     *   tags={"publishers"},
     *   summary="Publishers list",
     *   operationId="Publishers list",
     *   security={{ "apiAuth": {} }},
     *
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param  EloquentPublisherQueries  $eloquentPublisherQueries
     *
     * @return Publisher[]|Collection
     */
    public function list(EloquentPublisherQueries $eloquentPublisherQueries)
    {
        return $eloquentPublisherQueries->all();
    }
}
