<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class InitCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'init {--reset=false} {--seed=false}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Init';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        \Cache::flush();
        $reset = $this->option('reset') === 'true';
        $seed  = $this->option('seed') === 'true';

        if ($reset) {
            $this->call('migrate:fresh');
        } else {
            $this->call('migrate');
        }

        if ($seed) {
            $this->call('db:seed');
        }

        $this->call('admin:standard');

        return 0;
    }
}
