<?php

declare(strict_types=1);

namespace App\Services\Magazine;


use App\Models\Magazine;
use App\Services\Magazine\Contract\MagazineQueries;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class EloquentMagazineQueries
 * @package App\Services\Magazine
 */
final class EloquentMagazineQueries implements MagazineQueries
{

    /**
     * @param  MagazineSearchDto  $dto
     *
     * @return LengthAwarePaginator
     */
    public function filter(MagazineSearchDto $dto): LengthAwarePaginator
    {
        return Magazine::with(['publisher'])
            ->when($dto->getName(), fn(Builder $builder) => $builder
                ->where('name', 'like', "%{$dto->getName()}%"))
            ->when($dto->getPublisherId(), fn(Builder $builder) => $builder
                ->where('publisher_id', $dto->getPublisherId()))
            ->paginate(20);
    }

    /**
     * @param  int  $id
     *
     * @return Magazine|Builder|\Illuminate\Database\Eloquent\Model|object
     */
    public function getById(int $id): Magazine
    {
        $magazine = Magazine::with('publisher')
            ->where('id', $id)
            ->first();

        if ( ! $magazine) {
            abort(404);
        }

        return $magazine;
    }
}
