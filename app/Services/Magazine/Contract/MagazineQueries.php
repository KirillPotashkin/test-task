<?php

namespace App\Services\Magazine\Contract;

use App\Models\Magazine;
use App\Services\Magazine\MagazineSearchDto;

interface MagazineQueries
{
    public function filter(MagazineSearchDto $dto);

    public function getById(int $id): Magazine;
}
