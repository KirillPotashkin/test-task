<?php

declare(strict_types=1);

namespace App\Services\Magazine;

/**
 * Class MagazineSearchDto
 * @package App\Services\Magazine
 */
final class MagazineSearchDto
{

    /**
     * @var string|null
     */
    private ?string $name;


    /**
     * @var int|null
     */
    private ?int $publisher_id;

    private int $page;

    /**
     * MagazineSearchDto constructor.
     *
     * @param  string|null  $name
     * @param  int|null  $publisher_id
     * @param  int  $page
     */
    public function __construct(?string $name, ?int $publisher_id, int $page = 1)
    {
        $this->name         = $name;
        $this->publisher_id = $publisher_id;
        $this->page         = $page;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return int|null
     */
    public function getPublisherId(): ?int
    {
        return $this->publisher_id;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }
}
