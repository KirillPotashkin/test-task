<?php

declare(strict_types=1);

namespace App\Services\Magazine;


use App\Models\Magazine;
use App\Services\Magazine\Contract\MagazineQueries;
use Illuminate\Contracts\Cache\Repository;

final class CacheMagazineQueries implements MagazineQueries
{
    public const LASTS_DURATION = 10;

    /** @var MagazineQueries */
    private MagazineQueries $base;

    /** @var Repository */
    private Repository $cache;

    /**
     * CacheMagazineQueries constructor.
     *
     * @param  MagazineQueries  $base
     * @param  Repository  $cache
     */
    public function __construct(MagazineQueries $base, Repository $cache)
    {
        $this->base  = $base;
        $this->cache = $cache;
    }

    public function filter(MagazineSearchDto $dto)
    {
        $key = "magazine:name:{$dto->getName()}:publisher_id:{$dto->getPublisherId()}:page:{$dto->getPage()}";

        return $this->cache->remember($key, self::LASTS_DURATION,
            fn() => $this->base->filter($dto)
        );
    }

    public function getById(int $id): Magazine
    {
        return $this->cache->remember("magazine:$id", self::LASTS_DURATION,
            fn() => $this->base->getById($id)
        );
    }
}
