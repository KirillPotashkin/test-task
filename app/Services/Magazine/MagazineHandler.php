<?php

declare(strict_types=1);

namespace App\Services\Magazine;

use App\Models\Magazine;
use App\Services\Magazine\Contract\MagazineQueries;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MagazineHandler
 * @package App\Services\Magazine
 */
final class MagazineHandler
{
    /**
     * @var MagazineQueries
     */
    private MagazineQueries $magazineQueries;

    /**
     * MagazineHandler constructor.
     *
     * @param  MagazineQueries  $magazineQueries
     */
    public function __construct(MagazineQueries $magazineQueries)
    {
        $this->magazineQueries = $magazineQueries;
    }

    /**
     * @param  MagazineSearchDto  $dto
     *
     * @return LengthAwarePaginator
     */
    public function searchExecute(MagazineSearchDto $dto): LengthAwarePaginator
    {
        return $this->magazineQueries->filter($dto);
    }

    /**
     * @param  int  $id
     *
     * @return Magazine
     */
    public function getByIdExecute(int $id): Magazine
    {
        return $this->magazineQueries->getById($id);
    }
}
