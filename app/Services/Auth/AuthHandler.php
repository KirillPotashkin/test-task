<?php

declare(strict_types=1);

namespace App\Services\Auth;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

final class AuthHandler
{
    public function loginHandle(): JsonResponse
    {
        $credentials = request(['name', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], Response::HTTP_UNAUTHORIZED);
        }

        return $this->respondWithToken((string) $token);
    }

    public function meHandle(): JsonResponse
    {
        return response()->json(auth()->user());
    }

    public function logoutHandle(): JsonResponse
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function refreshHandle(): JsonResponse
    {
        return $this->respondWithToken(auth()->refresh());
    }


    private function respondWithToken(string $token): JsonResponse
    {
        return response()->json([
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => auth()->factory()->getTTL() * 60,
        ]);
    }
}
