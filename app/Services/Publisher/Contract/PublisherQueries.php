<?php


namespace App\Services\Publisher\Contract;


use App\Models\Publisher;

interface PublisherQueries
{
    public function all();
}
