<?php

declare(strict_types=1);

namespace App\Services\Publisher;

use App\Services\Publisher\Contract\PublisherQueries;
use Illuminate\Contracts\Cache\Repository;

final class CachePublisherQueries implements PublisherQueries
{

    public const LASTS_DURATION = 10;

    /** @var PublisherQueries */
    private PublisherQueries $base;

    /** @var Repository */
    private Repository $cache;

    /**
     * CachePublisherQueries constructor.
     *
     * @param  PublisherQueries  $base
     * @param  Repository  $cache
     */
    public function __construct(PublisherQueries $base, Repository $cache)
    {
        $this->base  = $base;
        $this->cache = $cache;
    }


    public function all()
    {
        return $this->cache
            ->remember(
                'publisher:all',
                self::LASTS_DURATION,
                fn() => $this->base->all()
            );
    }
}
