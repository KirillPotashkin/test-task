<?php

declare(strict_types=1);

namespace App\Services\Publisher;

use App\Models\Publisher;
use App\Services\Publisher\Contract\PublisherQueries;

final class EloquentPublisherQueries implements PublisherQueries
{

    public function all()
    {
        return Publisher::get(['id as value', 'name as text']);
    }
}
