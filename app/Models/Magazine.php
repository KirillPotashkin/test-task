<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @mixin IdeHelperMagazine
 */
class Magazine extends Model
{
    use HasFactory;

    public $timestamps = false;

    public function publisher(): BelongsTo
    {
        return $this->belongsTo(Publisher::class);
    }
}
