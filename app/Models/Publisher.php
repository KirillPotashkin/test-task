<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperPublisher
 */
class Publisher extends Model
{
    use HasFactory;

    public $timestamps = false;

    public function magazines(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Magazine::class);
    }
}
