<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\MagazineController;
use App\Http\Controllers\Api\PublisherController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix'     => 'authorize',
], static function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('me', [AuthController::class, 'me']);
});

Route::group([
    'middleware' => 'api',
    'prefix'     => 'publishers',
], static function () {
    Route::get('list', [PublisherController::class, 'list']);
});

Route::group([
    'middleware' => 'api',
    'prefix'     => 'magazines',
], static function () {
    Route::get('search', [MagazineController::class, 'search']);

    Route::get('/{id}', [MagazineController::class, 'show']);
});

