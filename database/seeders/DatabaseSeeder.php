<?php

namespace Database\Seeders;

use App\Models\Magazine;
use App\Models\Publisher;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::factory(10)->create();
        Publisher::factory()
            ->count(50)
            ->has(Magazine::factory()
                ->count(20)
            )
            ->create();
    }
}
